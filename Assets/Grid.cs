﻿using UnityEngine;
using System.Collections;

// Game of life (http://www.tiikoni.net/gameoflife/).
public class Grid : MonoBehaviour {

    public int cell1y = 5;
    public int cell1x = 5;

    public int cell2y = 5;
    public int cell2x = 6;

    public int cell3y = 6;
    public int cell3x = 4;

    public int cell4y = 6;
    public int cell4x = 5;

    public int cell5y = 3;
    public int cell5x = 4;

    public int cell6y = 4;
    public int cell6x = 4;

    public int cell7y = 5;
    public int cell7x = 1;

    public float nextCellUpdate = 0.0f;
    public float period = 0.1f;

    bool isPaused = false;

    public GameObject cellPrefab;	// Simulaationappula.
	public GameObject[,] cells; 	// Soluruudukko
	GameObject[,] tmpCells;			// Tilapäinen soluruudukko, johon jokaisen vaiheen muutokset tehdään ja jolla korvataan alkuperäinen ruudukko kun kaikki muutokset on laskettu ja tehty.

	public int sizeofCellGrid = 10; // Soluruudukon koko; oletuskoko 10*10.

    void OnGUI()
    {

        if (GUI.Button(new Rect(20, 40, 100, 20), "Play"))
        {
            
            Time.timeScale = 1;
            Debug.Log("PELI ALKANUT");
            isPaused = false;

        }
        if (GUI.Button(new Rect(20, 70, 100, 20), "Pause/resume"))
        {
            if (isPaused == false)
            {
                Time.timeScale = 0;
                isPaused = true;
                Debug.Log("Peli on pausella");

            }
            else if (isPaused == true)
            {
                Time.timeScale = 1;
                isPaused = false;
                Debug.Log("Peli on pois pauselta");
            }


        }
        if (GUI.Button(new Rect(20, 100, 100, 20), "Restart"))
        {
            Application.LoadLevel(Application.loadedLevel);
            Start();         

        }
        if (GUI.Button(new Rect(20, 130, 100, 20), "Step"))
        {
            Time.timeScale = 0;
            isPaused = true;
            updateCells();

        }
    }
        // Use this for initialization
        void Start () { 

		// Luodaan soluruudukot
		cells = new GameObject[sizeofCellGrid,sizeofCellGrid];
		tmpCells = new GameObject[sizeofCellGrid,sizeofCellGrid];

		FillCells ();
		setUpCells ();

        Time.timeScale = 0;
        isPaused = true;
    }
	

	// Update is called once per frame
	void Update () {

        // päivitetään solut automaattisesti tietyin väliajoin
        if (Time.time > nextCellUpdate && isPaused == false)
        {
            nextCellUpdate += period;
            updateCells();
        }
        
        
        
       /*if(isPaused == false )
        {            
            updateCells();
            
        }*/
       
        // Iteroidaan muutoksia askel kerrallaan tai jatkuvasti.
        if (Input.GetKeyDown(KeyCode.RightShift) || Input.GetKey(KeyCode.LeftShift))
		{
			updateCells();
			Debug.Log("Between turns");
		}
	}

	// Täytetään soluruudukot.
	void FillCells()
	{
		for (int i = 0; i < sizeofCellGrid; i++)
		for (int j = 0; j < sizeofCellGrid; j++) {
			cells [i,j] = (GameObject)Instantiate (cellPrefab, new Vector3 (i * 2, 0, j * 2), Quaternion.identity);
			tmpCells [i,j] = (GameObject)Instantiate (cellPrefab, new Vector3 (i * 2, 0, j * 2), Quaternion.identity);
			}
	}

	// Luodaan aloitussolut.
    void setUpCells()
	{
        
		Color color = cells [5,5].renderer.material.color;
		color.a = 1.0f;
        cells[cell1y, cell1x].renderer.material.color = color;
        cells[cell2y, cell2x].renderer.material.color = color;
        cells[cell3y, cell2x].renderer.material.color = color;
        cells[cell4y, cell4x].renderer.material.color = color;

        cells[cell5y, cell5x].renderer.material.color = color;
        cells[cell6y, cell6x].renderer.material.color = color;
        cells[cell7y, cell7x].renderer.material.color = color;
    }

	// Päivitetään solut.
	void updateCells()
	{
       
        // Jokaiselle solulle ... 
        for (int i = 0; i < sizeofCellGrid; i++)
			for (int j = 0; j < sizeofCellGrid; j++) 
			{
				// Siirretään aiemman iteraation oletustila muutoksia säilövään tilapäiseen soluruudukkoon.
				tmpCells[i,j].renderer.material.color = cells[i,j].renderer.material.color;

				// Lasketaan elävät ja kuolleet naapurit.
				int NumberOfDeadNeighbourCells = 0;
				int NumberOfAliveNeighbourCells = 0;

				for (int k = i-1; k <i+2; k++)
					for (int l = j-1; l < j+2; l++)
					{
				if (k >=0 && k < sizeofCellGrid && l >=0 && l < sizeofCellGrid && !(k==i && l==j))
						{
							if (cells[k,l].renderer.material.color.a < 1.0f)

								NumberOfDeadNeighbourCells++;
							if (cells[k,l].renderer.material.color.a == 1.0f)
								NumberOfAliveNeighbourCells++;
						}
					}
				
				// Päivitetään tarkasteltavan solun tilanne elävien ja kuolleiden naapurien mukaan ja säilötään tilapäiseen soluruudukkoon.
				if (cells[i,j].renderer.material.color.a < 1.0f)
					if (NumberOfAliveNeighbourCells == 3)
					{
						Color color = tmpCells[i,j].renderer.material.color;
						color.a = 1.0f;
						tmpCells[i,j].renderer.material.color = color;
						Debug.Log("Pawn is alive" + i + j);
					}
				if (cells[i,j].renderer.material.color.a == 1.0f)
					if (NumberOfAliveNeighbourCells < 2 || NumberOfAliveNeighbourCells > 3)
					{
						Color color = tmpCells[i,j].renderer.material.color;
						color.a = 0.1f;
						tmpCells[i,j].renderer.material.color = color;
						Debug.Log("Pawn is dead" + i + j);
					}
			}
       
        // Korvataan alkuperäisen solutaulukon solujen arvot tilapäisen solutaulukon arvoilla.
        for (int i = 0; i < sizeofCellGrid; i++)
            for (int j = 0; j < sizeofCellGrid; j++)
				cells[i,j].renderer.material.color = tmpCells[i,j].renderer.material.color;
       

    }

}
